# More Loading Screens

> Tired of the same old loading screens?<br>
> Fear no more, now there's plenty and they are randomized.

## Contributing

Did you draw an awesome Vermintide scene or did you manage to capture the perfect moment in a screenshot?<br>
Hit me up, I'll be happy to include your contributions.

## Stats

| Map                  | Loading Screens |
|----------------------|-----------------|
| Prologue             |               1 |
| Taal's Horn Keep     |               6 |
| Righteous Stand      |               9 |
| Concovation of Decay |               6 |
| Hunger in the Dark   |               4 |
| Halescourge          |               3 |
| Athel Yenlui         |              10 |
| Screaming Bell       |               6 |
| Fort Brachsenbrücke  |               2 |
| Into the Nest        |               7 |
| Against the Grain    |               5 |
| Empire in Flames     |              13 |
| Festering Grounds    |               8 |
| Warcamp              |               7 |
| Skittergate          |               3 |
| The Pit              |               6 |
| The Blightreaper     |               1 |
| Other                |               6 |
| **Total**            |             103 |
