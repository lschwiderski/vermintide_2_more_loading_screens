return {
	run = function()
		fassert(rawget(_G, "new_mod"), "'More Loading Screens' must be lower than Vermintide Mod Framework in your launcher's load order.")

		new_mod("more_loading_screens", {
			mod_script       = "scripts/mods/more_loading_screens/more_loading_screens",
			mod_data         = "scripts/mods/more_loading_screens/more_loading_screens_data",
			mod_localization = "scripts/mods/more_loading_screens/more_loading_screens_localization"
		})
	end,
	packages = {
		"resource_packages/more_loading_screens/more_loading_screens"
	}
}
