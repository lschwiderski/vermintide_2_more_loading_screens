--[[
  Copyright 2018 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("more_loading_screens")

return {
	name = "More Loading Screens",
	description = mod:localize("mod_description"),
  is_togglable = true,
  options = {
    widgets = {
      {
        setting_id = "show_credits",
        type = "checkbox",
        title = "option_show_credits",
        tooltip = "option_show_credits_tooltip",
        default_value = true,
        sub_widgets = {
          {
            setting_id = "credits_font_size",
            type = "numeric",
            title = "option_credits_font_size",
            tooltip = "option_credits_font_size_tooltip",
            range = { 10, 60 },
            default_value = 24,
          },
          {
            setting_id = "credits_position_group",
            type = "group",
            title = "option_credits_position_group",
            tooltip = "option_credits_position_group_tooltip",
            sub_widgets = {
              {
                setting_id = "credits_anchor_horizontal",
                type = "dropdown",
                title = "option_credits_anchor_horizontal",
                tooltip = "option_credits_anchor_horizontal_tooltip",
                options = {
                  { text = "option_anchor_top", value = "top" },
                  { text = "option_anchor_center", value = "center" },
                  { text = "option_anchor_bottom", value = "bottom" },
                },
                default_value = "bottom",
              },
              {
                setting_id = "credits_anchor_vertical",
                type = "dropdown",
                title = "option_credits_anchor_vertical",
                tooltip = "option_credits_anchor_vertical_tooltip",
                options = {
                  { text = "option_anchor_left", value = "left" },
                  { text = "option_anchor_center", value = "center" },
                  { text = "option_anchor_right", value = "right" },
                },
                default_value = "left",
              },
              {
                setting_id = "credits_position_x",
                type = "numeric",
                title = "option_credits_position_x",
                tooltip = "option_credits_position_x_tooltip",
                range = { -4000, 4000 },
                default_value = 10,
              },
              {
                setting_id = "credits_position_y",
                type = "numeric",
                title = "option_credits_position_y",
                tooltip = "option_credits_position_y_tooltip",
                range = { -3000, 3000 },
                default_value = 0,
              },
            },
          },
        },
      },
    },
  }
}
