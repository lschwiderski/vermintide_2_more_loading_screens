--[[
  Copyright 2018 Lucas Schwiderski

  Licensed under the Apache License,
  Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local vanilla_screens = {
  {
    package_id = "loading_screen_1",
    levels = { "bell" },
  },
  {
    package_id = "loading_screen_2",
    levels = { "mines" },
  },
  {
    package_id = "loading_screen_3",
    levels = { "skaven_stronghold" },
  },
  {
    package_id = "loading_screen_4",
    levels = { "catacombs" },
  },
  {
    package_id = "loading_screen_5",
    levels = { "nurgle" },
  },
  {
    package_id = "loading_screen_6",
    levels = { "elven_ruins" },
  },
  {
    package_id = "loading_screen_7",
    levels = { "farmlands" },
  },
  {
    package_id = "loading_screen_8",
    levels = { "fort" },
  },
  {
    package_id = "loading_screen_9",
    levels = { "prologue" },
  },
  {
    package_id = "loading_screen_10",
    levels = { "ground_zero" },
  },
  {
    package_id = "loading_screen_11",
    levels = { "ussingen" },
  },
  {
    package_id = "loading_screen_12",
    levels = { "warcamp" },
  },
  {
    package_id = "loading_screen_13",
    levels = { "military" },
  },
  {
    package_id = "loading_screen_14",
    levels = { "skittergate" },
  },
  {
    package_id = "loading_screen_15",
    levels = { "inn_level" },
  },
  {
    package_id = "loading_screen_16",
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    package_id = "loading_screen_17",
    levels = { "dlc_bogenhafen_city" },
  },
  {
    package_id = "loading_screen_18",
    levels = { "forest_ambush" },
  },
  {
    package_id = "loading_screen_19",
    levels = { "magnus" },
  },
  {
    package_id = "loading_screen_20",
    levels = { "cemetery" },
  },
  {
    package_id = "loading_screen_21",
    levels = { "plaza" },
  },
}

local screens = {
  {
    texture = "skacikpl_1",
    credits = { name = "SkacikPL" },
  },
  {
    texture = "skacikpl_2",
    credits = { name = "SkacikPL" },
  },
  {
    texture = "skacikpl_3",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_4",
    credits = { name = "SkacikPL" },
    levels = { "ussingen" },
  },
  {
    texture = "skacikpl_5",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    texture = "skacikpl_6",
    credits = { name = "SkacikPL" },
    levels = { "farmlands" },
  },
  {
    texture = "skacikpl_7",
    credits = { name = "SkacikPL" },
    levels = { "elven_ruins" },
  },
  {
    texture = "skacikpl_8",
    credits = { name = "SkacikPL" },
    levels = { "farmlands" },
  },
  {
    texture = "skacikpl_9",
    credits = { name = "SkacikPL" },
    levels = { "ussingen" },
  },
  {
    texture = "skacikpl_10",
    credits = { name = "SkacikPL" },
    levels = { "ussingen" },
  },
  {
    texture = "skacikpl_11",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    texture = "skacikpl_12",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    texture = "skacikpl_13",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    texture = "skacikpl_14",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_slum" },
  },
  {
    texture = "skacikpl_15",
    credits = { name = "SkacikPL" },
    levels = { "catacombs" },
  },
  {
    texture = "skacikpl_16",
    credits = { name = "SkacikPL" },
    levels = { "elven_ruins" },
  },
  {
    texture = "skacikpl_17",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_18",
    credits = { name = "SkacikPL" },
    levels = { "bell" },
  },
  {
    texture = "skacikpl_19",
    credits = { name = "SkacikPL" },
    levels = { "mines" },
  },
  {
    texture = "skacikpl_20",
    credits = { name = "SkacikPL" },
    levels = { "elven_ruins" },
  },
  {
    texture = "skacikpl_21",
    credits = { name = "SkacikPL" },
  },
  {
    texture = "skacikpl_22",
    credits = { name = "SkacikPL" },
  },
  {
    texture = "skacikpl_23",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_24",
    credits = { name = "SkacikPL" },
    levels = { "bell" },
  },
  {
    texture = "skacikpl_25",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_26",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_27",
    credits = { name = "SkacikPL" },
    levels = { "inn_level" },
  },
  {
    texture = "skacikpl_28",
    credits = { name = "SkacikPL" },
    levels = { "skittergate" },
  },
  {
    texture = "skacikpl_29",
    credits = { name = "SkacikPL" },
    levels = { "elven_ruins" },
  },
  {
    texture = "skacikpl_30",
    credits = { name = "SkacikPL" },
    levels = { "dlc_bogenhafen_city" }
  },
  {
    texture = "fs_bell_1",
    credits = { name = "Fatshark" },
    levels = { "bell" },
  },
  {
    texture = "fs_bell_2",
    credits = { name = "Fatshark" },
    levels = { "bell" },
  },
  {
    texture = "fs_bell_3",
    credits = { name = "Fatshark" },
    levels = { "bell" },
  },
  {
    texture = "fs_catacombs_1",
    credits = { name = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "fs_catacombs_2",
    credits = { name = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "fs_catacombs_3",
    credits = { name = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "fs_catacombs_4",
    credits = { name = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "fs_elven_ruins_1",
    credits = { name = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "fs_elven_ruins_2",
    credits = { name = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "fs_elven_ruins_3",
    credits = { name = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "fs_elven_ruins_4",
    credits = { name = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "fs_elven_ruins_5",
    credits = { name = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "fs_farmlands_1",
    credits = { name = "Fatshark" },
    levels = { "farmlands" },
  },
  {
    texture = "fs_farmlands_2",
    credits = { name = "Fatshark" },
    levels = { "farmlands" },
  },
  {
    texture = "fs_farmlands_3",
    credits = { name = "Fatshark" },
    levels = { "farmlands" },
  },
  {
    texture = "fs_fort_1",
    credits = { name = "Fatshark" },
    levels = { "fort" },
  },
  {
    texture = "fs_ground_zero_1",
    credits = { name = "Fatshark" },
    levels = { "ground_zero" },
  },
  {
    texture = "fs_ground_zero_2",
    credits = { name = "Fatshark" },
    levels = { "ground_zero" },
  },
  {
    texture = "fs_military_1",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_2",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_3",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_4",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_5",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_6",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_7",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_military_8",
    credits = { name = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "fs_mines_1",
    credits = { name = "Fatshark" },
    levels = { "mines" },
  },
  {
    texture = "fs_mines_2",
    credits = { name = "Fatshark" },
    levels = { "mines" },
  },
  -- {
  --   texture = "fs_nurgle_1",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_2",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_3",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_4",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_5",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_6",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  -- {
  --   texture = "fs_nurgle_7",
  --   credits = { name = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  {
    texture = "fs_skaven_stronghold_1",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skaven_stronghold_2",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skaven_stronghold_3",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skaven_stronghold_4",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skaven_stronghold_5",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skaven_stronghold_6",
    credits = { name = "Fatshark" },
    levels = { "skaven_stronghold" },
  },
  {
    texture = "fs_skittergate_1",
    credits = { name = "Fatshark" },
    levels = { "skittergate" },
  },
  {
    texture = "fs_ussingen_1",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_2",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_3",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_4",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_5",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_6",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_7",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_8",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_ussingen_9",
    credits = { name = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "fs_warcamp_1",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_warcamp_2",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_warcamp_3",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_warcamp_4",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_warcamp_5",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_warcamp_6",
    credits = { name = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "patrik-rosander-athel-yenlui-undergrowth-tree-vines",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "patrik-rosander-athel-yenlui-undergrowth-vines-destructible-01",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "patrik-rosander-athel-yenlui-undergrowth-vines-destructible-02",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "patrik-rosander-athel-yenlui-undergrowth-vines-destructible-03",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "patrik-rosander-chaos-warcamp-arena-lines",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "patrik-rosander-chaos-warcamp-gate-v2",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "patrik-rosander-convocation-of-decay-catacombs-ceremony-02",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "patrik-rosander-convocation-of-decay-ceremony-01",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "patrik-rosander-convocation-of-decay-shallya-entrance",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "catacombs" },
  },
  {
    texture = "patrik-rosander-fort-brachsenbrucke-siege-vista",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "fort" },
  },
  {
    texture = "patrik-rosander-halescourge-ending",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "ground_zero" },
  },
  {
    texture = "patrik-rosander-helmgart-prop-cloth-bale",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  {
    texture = "patrik-rosander-helmgart-prop-empire-metal-crate",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  {
    texture = "patrik-rosander-helmgart-prop-fireplates01",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  {
    texture = "patrik-rosander-helmgart-prop-fireplates-smoke",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  {
    texture = "patrik-rosander-helmgart-prop-jugs-callout",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  -- {
  --   texture = "patrik-rosander-high-elf-ruins-puzzle-finish",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "elven_ruins" },
  -- },
  {
    texture = "patrik-rosander-high-elf-ruins-puzzle-start",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "elven_ruins" },
  },
  {
    texture = "patrik-rosander-hunger-in-the-dark-vista",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "mines" },
  },
  -- {
  --   texture = "patrik-rosander-leyline-hub-entrance",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "elven_ruins" },
  -- },
  -- {
  --   texture = "patrik-rosander-leyline-hub-plains",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "elven_ruins" },
  -- },
  -- {
  --   texture = "patrik-rosander-leyline-hub-spell",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "elven_ruins" },
  -- },
  -- {
  --   texture = "patrik-rosander-med-kit",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  -- },
  -- {
  --   texture = "patrik-rosander-mines-minecart",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "mines" },
  -- },
  -- {
  --   texture = "patrik-rosander-norsca-camp",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "skittergate" },
  -- },
  -- {
  --   texture = "patrik-rosander-norsca-vista",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "skittergate" },
  -- },
  -- {
  --   texture = "patrik-rosander-nurgle-swamp-prez",
  --   credits = { name = "Patrik Rosander", url = "Fatshark" },
  --   levels = { "nurgle" },
  -- },
  {
    texture = "patrik-rosander-righteous-stand-helmgart-wall",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "military" },
  },
  {
    texture = "patrik-rosander-skulls-wallpaper-4k",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
  },
  {
    texture = "patrik-rosander-ussingen-plaza",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "ussingen" },
  },
  {
    texture = "patrik-rosander-warcamp-gate",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "patrik-rosander-warcamp-tent-01",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "patrik-rosander-warcamp-tent-02",
    credits = { name = "Patrik Rosander", url = "Fatshark" },
    levels = { "warcamp" },
  },
  {
    texture = "fs_cemetery_1",
    credits = { name = "Fatshark" },
    levels = { "cemetery" },
  },
  {
    texture = "fs_forest_ambush_1",
    credits = { name = "Fatshark" },
    levels = { "forest_ambush" },
  },
  {
    texture = "fs_forest_ambush_2",
    credits = { name = "Fatshark" },
    levels = { "forest_ambush" },
  },
  {
    texture = "fs_forest_ambush_3",
    credits = { name = "Fatshark" },
    levels = { "forest_ambush" },
  },
  {
    texture = "fs_magnus_1",
    credits = { name = "Fatshark" },
    levels = { "magnus" },
  },
  {
    texture = "fs_magnus_2",
    credits = { name = "Fatshark" },
    levels = { "magnus" },
  },
  {
    texture = "fs_magnus_3",
    credits = { name = "Fatshark" },
    levels = { "magnus" },
  },
  {
    texture = "fs_magnus_4",
    credits = { name = "Fatshark" },
    levels = { "magnus" },
  },
}

return {
  vanilla = vanilla_screens,
  mod = screens,
}
