--[[
  Copyright 2018 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

return {
	mod_description = {
		en = "Tired of the same old loading screens?\nFear no more, now there's plenty and they are randomized."
  },
  option_show_credits = {
    en = "Show Image Credits"
  },
  option_show_credits_tooltip = {
    en = ""
  },
  option_credits_font_size = {
    en = "Font Size"
  },
  option_credits_font_size_tooltip = {
    en = "This option requires an application restart to take effect."
  },
  option_credits_position_group = {
    en = "Credits Position"
  },
  option_credits_position_group_tooltip = {
    en = ""
  },
  option_credits_position_x = {
    en = "X"
  },
  option_credits_position_x_tooltip = {
    en = "The horizontal position in pixels. Based on anchor position. This option requires an application restart to take effect."
  },
  option_credits_position_y = {
    en = "Y"
  },
  option_credits_position_y_tooltip = {
    en = "The vertical position in pixels. Based on anchor position. This option requires an application restart to take effect."
  },
  option_credits_anchor_horizontal = {
    en = "Horizontal Anchor"
  },
  option_credits_anchor_horizontal_tooltip = {
    en = "The horizontal screen anchor on which to base the horizontal and vertical offset values. This option requires an application restart to take effect."
  },
  option_credits_anchor_vertical = {
    en = "Vertical Anchor"
  },
  option_credits_anchor_vertical_tooltip = {
    en = "The vertical screen anchor on which to base the horizontal and vertical offset values. This option requires an application restart to take effect."
  },
  option_anchor_top = {
    en = "Top"
  },
  option_anchor_center = {
    en = "Center"
  },
  option_anchor_bottom = {
    en = "Bottom"
  },
  option_anchor_left = {
    en = "Left"
  },
  option_anchor_right = {
    en = "Right"
  },
}
