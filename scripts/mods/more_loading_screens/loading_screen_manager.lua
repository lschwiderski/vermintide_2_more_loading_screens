--[[
  Copyright 2018 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("more_loading_screens")

local LEVEL_IDS = {
  "prologue", -- Tutorial
  "military", -- Righteous Stand
  "catacombs", -- Convocation Of Decay
  "mines", -- Hunger in the Dark
  "ground_zero", -- Halescourge
  "elven_ruins", -- Athel Yenlui
  "bell", -- Screaming Bell
  "fort", -- Fort Brachsenbruecke
  "skaven_stronghold", -- Into the Nest
  "farmlands", -- Against the Grain
  "ussingen", -- Empire in Flames
  "nurgle", -- Festering Ground
  "warcamp", -- Warcamp
  "skittergate", -- Skittergate
  "dlc_bogenhafen_slum", -- The Pit
  "dlc_bogenhafen_city", -- The Blightreaper
  "inn_level", -- Taal's Horn Keep
  "magnus", -- Horn of Magnus
  "cemetery", -- Garden of Morr
  "forest_ambush", -- Engines of War
  "plaza", -- Fotrunes of War
}

LoadingScreenManager = class(LoadingScreenManager)

function LoadingScreenManager:init()
  self._screens = {}

  local lookups = {
    all = {},
    levels = {},
    no_level = {}
  }

  for _, level_id in ipairs(LEVEL_IDS) do
    lookups.levels[level_id] = {}
  end

  self._lookups = lookups

  self._current_screen_id = nil
  self._previous_screen_ids = {}
end

function LoadingScreenManager:register(screen)
  local lookups = self._lookups
  local id = #self._screens + 1

  self._screens[id] = screen

  table.insert(lookups.all, id)

  if screen.levels then
    for _, level_id in ipairs(screen.levels) do
      table.insert(lookups.levels[level_id], id)
    end
  else
    table.insert(lookups.no_level, id)
  end
end

function LoadingScreenManager:next(level_id)
  local lookups = self._lookups
  local active_screens = {}

  local level_lookup = level_id and lookups.levels[level_id]
  if level_lookup then
    -- if level_id == "inn_level" then
    --   table.append(active_screens, lookups.no_level)
    -- end

    table.append(active_screens, level_lookup)
  else
    table.append(active_screens, lookups.all)
  end

  local num_active_screens = #active_screens

  if num_active_screens == 0 then
    table.append(active_screens, lookups.all)
    num_active_screens = #active_screens
  end
  local next_screen_id
  local previous_screen_id = self._previous_screen_ids[level_id]

  repeat
    next_screen_id = active_screens[math.random(num_active_screens)]
  until next_screen_id ~= previous_screen_id

  local screen = self._screens[next_screen_id]

  if not screen then
    mod:error("Couldn't get screen for id '%s'. %s screens active", next_screen_id, num_active_screens)
    self._current_screen_id = 1
    return self._screens[1]
  end

  mod:debug("Selected texture %s as next loading screen", screen.texture_id)

  self._current_screen_id = next_screen_id
  self._previous_screen_ids[level_id] = next_screen_id
  return screen
end

function LoadingScreenManager:get_current()
  return self._screens[self._current_screen_id]
end
