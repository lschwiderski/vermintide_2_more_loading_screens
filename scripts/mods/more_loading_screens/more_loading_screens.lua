--[[
  Copyright 2018 Lucas Schwiderski

  Licensed under the Apache License,
  Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("more_loading_screens")

local scenegraph_definition = {
  credits = {
    vertical_alignment = mod:get("credits_anchor_horizontal") or "bottom",
		parent = "background_image",
		horizontal_alignment = mod:get("credits_anchor_vertical") or "left",
		size = {
			520,
			(mod:get("credits_font_size") or 24) * 1.2,
		},
		position = {
			mod:get("credits_position_x") or 10,
			mod:get("credits_position_y") or 0,
			4,
		},
  },
}

local loading_view_definitions = require("scripts/ui/views/loading_view_definitions")

table.merge(loading_view_definitions.scenegraph_definition, scenegraph_definition)

loading_view_definitions.credits_widget = UIWidgets.create_simple_text("", "credits", nil, nil, {
  vertical_alignment = mod:get("credits_anchor_horizontal") or "bottom",
  horizontal_alignment = mod:get("credits_anchor_vertical") or "left",
  font_type = "hell_shark_header",
  font_size = mod:get("credits_font_size") or 24,
  text_color = Colors.get_color_table_with_alpha("font_default", 180),
  offset = { 0, 0, 2 },
})

dofile("scripts/mods/more_loading_screens/loading_screen_manager")
local screen_definitions = dofile("scripts/mods/more_loading_screens/screen_definitions")

local manager = LoadingScreenManager:new()

local none_atlas_textures = {}

function mod:register_screen(screen)
  local texture_id = screen.texture_id

  if screen.package_type ~= "vanilla" then
    if none_atlas_textures[texture_id] then
      mod:warning("Loading screen with texture id '%s' already exists. Choose a different id.", texture_id)
      return
    end

    none_atlas_textures[texture_id] = true
  end

  manager:register(screen)
end

for _, screen in ipairs(screen_definitions.vanilla) do
  mod:register_screen({
    texture_id = "loading_screen",
    bg_material = "materials/ui/loading_screens/" .. screen.package_id,
    package_name = "resource_packages/loading_screens/" .. screen.package_id,
    credits = { name = "Fatshark" },
    levels = screen.levels,
    package_type = "vanilla",
  })
end

for _, screen in ipairs(screen_definitions.mod) do
  local texture_id = screen.texture

  mod:register_screen({
    texture_id = texture_id,
    bg_material = "materials/mods/more_loading_screens/" .. texture_id,
    package_name = "resource_packages/more_loading_screens/" .. texture_id,
    credits = screen.credits,
    levels = screen.levels,
  })
end

mod:hook(UIAtlasHelper, "get_atlas_settings_by_texture_name", function(func, texture_name)
  if none_atlas_textures[texture_name] then
    return
  end

  return func(texture_name)
end)

mod:hook_origin(StateLoading, "setup_loading_view", function (self, level_key)
	self._level_key = level_key or self._level_transition_handler:default_level_key()
  self._loading_view_setup_is_done = true

  local package_manager = Managers.package

  if self._ui_package_name then
    if self._ui_package_type == "vanilla" then
      if package_manager:has_loaded(self._ui_package_name, "global_loading_screens") or package_manager:is_loading(self._ui_package_name) then
        package_manager:unload(self._ui_package_name, "global_loading_screens")
      end
    else
      mod:unload_package(self._ui_package_name)
    end
  end

  local screen = manager:next(self._level_key)

  self._ui_package_name = screen.package_name
  local act_progression_index = nil
  if screen.package_type == "vanilla" then
    mod:debug("Loading standard package '%s'", self._ui_package_name)
    self._ui_package_type = "standard"

    if not package_manager:has_loaded(self._ui_package_name) and not package_manager:has_loaded(self._ui_package_name, "global_loading_screens") then
      package_manager:load(
        self._ui_package_name,
        "global_loading_screens",
        callback(self, "cb_loading_screen_loaded", self._level_key, act_progression_index),
        true,
        true
      )
    else
      -- The required package was already loaded
      self:cb_loading_screen_loaded(self._level_key, act_progression_index, true)
    end
  else
    mod:debug("Selected mod package '%s'. Current status: %s", self._ui_package_name, mod:package_status(self._ui_package_name) or "nil")
    self._ui_package_type = "mod"

    if mod:package_status(self._ui_package_name) ~= "loaded" then
      mod:debug("Loading mod package '%s'", self._ui_package_name)
      mod:load_package(
        self._ui_package_name,
        callback(self, "cb_loading_screen_loaded", self._level_key, act_progression_index)
      )
    else
      -- The required package was already loaded
      self:cb_loading_screen_loaded(self._level_key, act_progression_index, true)
    end
  end
end)

mod:hook(StateLoading, "cb_loading_screen_loaded", function(func, self, ...)
  mod:debug("[StateLoading.cb_loading_screen_loaded] Called. Package status: %s", mod:package_status(self._ui_package_name) or "nil")
  return func(self, ...)
end)

mod:hook_origin(LoadingView, "texture_resource_loaded", function(self, level_key, act_progression_index, game_difficulty)
  if self.return_to_pc_menu then
		return
	end

	UIRenderer.destroy(self.ui_renderer, self.world)

  local screen = manager:get_current()
  local credits = screen.credits

  self.bg_widget.content.bg_texture = screen.texture_id
  if mod:get("show_credits") and credits and credits.name then
    local text = credits.name

    if credits.source then
      text = text .. " - " .. credits.source
    end
    self.credits_widget.content.text = text
    self.credits_widget.style.visible = true
  else
    self.credits_widget.content.visible = false
  end

  self.ui_renderer = UIRenderer.create(
    self.world,
    "material",
    "materials/ui/loading_screens/" .. self.default_loading_screen,
    "material",
    screen.bg_material,
    "material",
    "materials/fonts/gw_fonts",
    "material",
    "materials/ui/ui_1080p_common",
    "material",
    "materials/ui/ui_1080p_hud_atlas_textures",
    "material",
    "materials/ui/ui_1080p_chat"
  )

	self.level_key = level_key
	self.act_progression_index = act_progression_index

	local level_settings = LevelSettings[level_key]
	if level_key ~= "inn_level" and level_settings.level_type ~= "survival" then
		self:setup_act_text(level_key)
		self:setup_difficulty_text(game_difficulty)
	end

	self:setup_level_text(level_key)
	self:setup_tip_text(act_progression_index, level_settings.game_mode or "adventure")
end)

mod:hook_safe(LoadingView, "create_ui_elements", function(self)
  self.credits_widget = UIWidget.init(loading_view_definitions.credits_widget)

  table.insert(self.widgets, self.credits_widget)
end)

